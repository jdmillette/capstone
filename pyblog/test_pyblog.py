import pyblog
import pytest
import requests
import io


class MockResponse:
    def __init__(self, data, status_code):
        self.data = data
        self.status_code = status_code

    def json(self):
        return self.data

    def raise_for_status(self):
        return self.status_code


def test_get_auth_token():
    header = pyblog.get_auth_token("username", "password")
    # The value with basic is base64 encode of username:password
    assert header == {"Authorization": "Basic dXNlcm5hbWU6cGFzc3dvcmQ="}


def mock_read_data(*args, headers={"Authorization": "Basic 1234"}, timeout=5):
    data = [
        {
            "title": {"rendered": "Wordpress Title"},
            "content": {"rendered": "Wordpress Content"},
        }
    ]
    return MockResponse(data, 200)


def test_read_pass(mocker):
    mocker.patch("requests.get", mock_read_data)
    post = pyblog.read("wordpress_username", "wordpress_password", "wordpress_url")

    assert post["title"] == "Wordpress Title"
    assert post["body"] == "Wordpress Content"


def mock_read_data_failure_timeout(
    *args, headers={"Authorization": "Basic 1234"}, timeout=5
):
    raise requests.ConnectionError


def mock_read_data_failure_httperror(
    *args, headers={"Authorization": "Basic 1234"}, timeout=5
):

    raise requests.HTTPError


def test_read_fail_timeout(mocker):
    """
        This tests makes sure that ConnectionError is caught """

    mocker.patch("requests.get", mock_read_data_failure_timeout)
    pyblog.read("wordpress_username", "wordpress_password", "wordpress_url")
    assert requests.ConnectionError


def test_read_fail_tiomeout(mocker):
    """
        This test makes sure any HTTPError as caught """
    mocker.patch("requests.get", mock_read_data_failure_timeout)
    pyblog.read("wordpress_username", "wordpress_password", "wordpress_url")
    assert requests.HTTPError


def mock_upload(*args, headers={"Authorization": "Basic 1234"}, timeout=5):
    """
        Test data for a successful response """
    data = ""
    response = 201
    return MockResponse(data, response)


def mock_upload_file(*args):
    """
        Mock data to pass to check that file being uploaded is a file """
    isfile = True
    return isfile


def mock_file(*args):
    """
        Mocks a file like obejct for open """
    content = io.StringIO("Hello, World")
    return content


def test_upload_succesful_call(mocker):
    mocker.patch("requests.post", mock_upload)
    mocker.patch("os.path.isfile", mock_upload_file)
    mocker.patch("builtins.open", mock_file)

    status = pyblog.upload(
        "wordpress_username",
        "wordpress_password",
        "wordpress_url",
        "filename",
        "blog_post_title",
    )

    # assert os.path.isfile.called_once()
    # assert content == 'Hello, World'
    assert status == 201


def mocker_failed_upload_timeout(
    *args, headers={"Authorization": "Basic 1234"}, timeout=5
):
    """Raises a timeout error """
    raise requests.ConnectionError


def test_failed_upload_timeout(mocker):
    mocker.patch("requests.post", mock_upload)
    mocker.patch("os.path.isfile", mock_upload_file)
    mocker.patch("builtins.open", mock_file)

    pyblog.upload(
        "wordpress_username",
        "wordpress_password",
        "wordpress_url",
        "filename",
        "blog_post_title",
    )

    assert requests.ConnectionError


def mock_upload_file_false(*args):
    """
        Mock data to say file is not an actual file """
    isfile = False
    return isfile


def test_upload_no_file(mocker):
    """
     Tests that the file trying to be uploaded is not an actual file. FileNotFound error should be raised
    """
    mocker.patch("requests.post", mock_upload)
    mocker.patch("os.path.isfile", mock_upload_file_false)
    mocker.patch("builtins.open", mock_file)

    with pytest.raises(FileNotFoundError):
        assert pyblog.upload(
            "wordpress_username",
            "wordpress_password",
            "wordpress_url",
            "filename",
            "blog_post_title",
        )


def mocker_failed_upload_wrong_creds(
    *args, headers={"Authorization": "Basic 1234"}, timeout=5
):
    """Raises a timeout error """
    raise requests.HTTPError


def test_failed_upload__invalid_creds(mocker):
    mocker.patch("requests.post", mock_upload)
    mocker.patch("os.path.isfile", mock_upload_file)
    mocker.patch("builtins.open", mock_file)

    pyblog.upload(
        "wordpress_username",
        "wordpress_password",
        "wordpress_url",
        "filename",
        "blog_post_title",
    )

    assert requests.HTTPError


def test_main_with_args_read(mocker):
    mocker.patch("requests.get", mock_read_data)
    return_value = pyblog.main(["read"])

    assert return_value["title"] == "Wordpress Title"
    assert return_value["body"] == "Wordpress Content"


def test_main_args_upload(mocker):
    mocker.patch("requests.post", mock_upload)
    mocker.patch("os.path.isfile", mock_upload_file)
    mocker.patch("builtins.open", mock_file)
    return_value = pyblog.main(["upload", "-f", "test", "-t", "Blog"])

    assert return_value == 201
