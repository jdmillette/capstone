# README #



### What is this repository for? ###

This repository is the capstone project for IEA cohort 3.
It uses docker, ansible, python and wordpress. Within this repo, you will see a python application that uses the wordpress api to create blog posts and pull down and read
the latest blog post from a site. 

### How do I get set up? ###

To get setup, you first need to run cft file under aws. This will create three EC2s and two security groups. One EC2 will be the for the infrastructure and the other two will serve
as a staging and production serve.

You will then want to run the bellow code on your infrastructure node to install bamboo.

docker volume create --name bambooVolume

docker run --group-add $(getent group docker | cut -d ":" -f 3) -v /var/run/docker.sock:/var/run/docker.sock -v bambooVolume:k --name="bamboo" --init -d -p 54663:54663 -p 8085:8085 jrrickerson/capstone-bamboo

You will then want to run the playbook.yaml to install the depenecies on the EC2s. After the playbook runs, you will want to run the docker-compose.yaml under wordpress.
This will install wordpress and a mysql database.

For the docker-compose file, you will need to create .env file to store the environment variables you need to run the docker compose via bamboo plan and run the dock-env-playbook under ansible.

In order to run the pyblog.py sript, you will need to set three environment variables both locally and on the servers you want to run. These are the environment variables you will need to set: USERNAME, PASSWORD, ENVIRONMENT. This allows you to run the application without passing sensitive information. 

Once the enrvironment variables are set, you should be able to call pyblog.py read to get the lastest blog post. If you want to upload a file, you can call pyblog.py -f <file location> -t <tittle> or pyblog.py -f - -t <tittle> < <file location> to read from standard input. 

